package com.example.khaicoder.download.precenter;

import com.example.khaicoder.download.model.Picture;

import java.io.IOException;
import java.util.List;

public interface IDownLoad {
    void downloadImage(String url) throws IOException;
    void getmodel();
    void cancelDownload();
}

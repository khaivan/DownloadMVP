package com.example.khaicoder.download.model;

import java.util.List;

public interface ILoadImage {
    List<Picture> getPath();
}

package com.example.khaicoder.download.precenter;

import android.os.Handler;
import android.os.Message;
import android.renderscript.ScriptGroup;
import android.util.Log;

import com.example.khaicoder.download.Utils;
import com.example.khaicoder.download.model.LoadImage;
import com.example.khaicoder.download.model.Picture;
import com.example.khaicoder.download.view.IGetPath;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class DownLoad implements IDownLoad {
    private LoadImage loadImage = new LoadImage();
    private IGetPath iGetPath;
    private File file;
    private List<Picture> path = new ArrayList<>();
    private Handler handler;
    private String fileName;
    public DownLoad(IGetPath iGetPath) {
        this.iGetPath = iGetPath;
    }

    private ExecutorService executorService;
    private Future future;

    @Override
    public void downloadImage(String urlSpec) {
        executorService = Executors.newFixedThreadPool(1);
        Runnable task1 = () -> {
            try {
                dowload(urlSpec);
                Message message = new Message();
                message.what = Utils.ID;
                handler.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }

        };
        future = executorService.submit(task1);
    }

    @Override
    public void getmodel() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == Utils.ID) {
                    iGetPath.setPath(loadImage.getPath());
                }
            }
        };
    }

    @Override
    public void cancelDownload() {
        if (future != null) {
            future.cancel(true);
            File f = new File(Utils.FILE_PATH);
            for (File file : f.listFiles()
                    ) {
                if (file.getName().equals(fileName)) {
                    file.delete();
                }
            }

        }

    }

    private void dowload(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            File file = new File(Utils.FILE_PATH);
            if (!file.isDirectory()) {
                file.mkdir();
            }
            fileName = urlSpec.substring(urlSpec.lastIndexOf("/"),urlSpec.length())+ ".png";
            File f = new File(Utils.FILE_PATH, fileName);

            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return;
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            FileOutputStream fileOutputStream = new FileOutputStream(f);
            ;
            while ((bytesRead = in.read(buffer)) != -1) {

                fileOutputStream.write(buffer, 0, bytesRead);
            }
            fileOutputStream.close();


        } finally {
            connection.disconnect();
        }
    }

}

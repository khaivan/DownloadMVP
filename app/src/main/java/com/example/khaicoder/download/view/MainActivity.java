package com.example.khaicoder.download.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.khaicoder.download.R;
import com.example.khaicoder.download.Utils;
import com.example.khaicoder.download.model.Picture;
import com.example.khaicoder.download.precenter.DownLoad;
import com.example.khaicoder.download.precenter.IDownLoad;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements IGetPath {
    private DownLoad downLoad;
    private ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        img = findViewById(R.id.img);

        downLoad = new DownLoad(this);
        downLoad.downloadImage("https://pets.com.vn/wp-content/uploads/2017/06/hinh-anh-meo-con-de-thuong-43.jpg");
        downLoad.getmodel();
    }


    @Override
    public void setPath(List<Picture> path) {

        for (Picture picture : path
                ) {
            Glide.with(this)
                    .load(picture.getPath())
                    .into(img);
            Toast.makeText(this, "" + picture.getPath(), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        downLoad.cancelDownload();
    }
}

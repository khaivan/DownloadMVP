package com.example.khaicoder.download.model;

import com.example.khaicoder.download.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LoadImage implements ILoadImage {
    private File file;
private List<Picture> list;

    private void load() {
        list = new ArrayList<>();
        File fileName = new File(Utils.FILE_PATH);
        if (fileName.listFiles() == null) {
            return;
        }
        for (File f : fileName.listFiles()
                ) {

            file = new File(Utils.FILE_PATH + f.getName());


            list.add(new Picture(new File(f.getPath()), f.getName()));

            for (int i = 0; i < list.size(); i++) {
                for (int j = list.size() - 1; j > i; j--) {
                    if (list.get(j).getName().equals(list.get(i).getName())) {
                        list.remove(j);
                    }
                }

            }

        }
    }


    @Override
    public List<Picture> getPath() {
        load();
        return list;
    }
}
